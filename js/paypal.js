(function (jQuery, settings) {
  Drupal.behaviors.paid_ads_paypal_gateway = {
    attach: function (context) {
      jQuery('.paypal-button', context).each(function (key, element) {
        paypal.Buttons({
          style: settings.paypal_buttons,
          createOrder: function (data, actions) {
            return fetch('/api/paid/create', {
              method: 'post',
              headers: new Headers({
                'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
              }),
              body: new URLSearchParams({
                type: 'paypal_gateway',
                options: JSON.stringify({
                  data_entity_type: element.getAttribute('data-entity-type'),
                  data_bundle: element.getAttribute('data-bundle'),
                  data_field: element.getAttribute('data-field'),
                  data_entity_id: element.getAttribute('data-entity-id'),
                  data_field_value: element.getAttribute('data-field-value'),
                })
              }),
            }).then(function (res) {
              return res.json();
            }).then(function (data) {
              return data.id;
            });
          },
          onApprove: function (data, actions) {
            actions.order.capture().then(function (details) {
              fetch('/api/paid/execute', {
                method: 'post',
                headers: new Headers({
                  'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }),
                body: new URLSearchParams({
                  paymentID: data.orderID,
                  payerID: data.payerID,
                  type: 'paypal_gateway',
                })
              }).then(function (res) {
                window.location.reload()
              });
            });

          },
        }).render(element);
      })
    }
  }
})(jQuery, drupalSettings);
