# PAID ADS

## INTRODUCTION
This module provides you with a field which allows users to make payments by
simply clicking on it. It allows you to configure the amount of payment and a
payment gateway (only PayPal is supported by now) to use for each particular
field.

## REQUIREMENTS
Drupal core. No external modules or libraries are required.

## INSTALLATION
`composer require drupal/paid_ads` or any other standard way of installing a
drupal module.

## CONFIGURATION
First, you need to fill up the credentials and any other required data for each
payment gateway you wish to use.
After that, add a `Paid (Boolean)` field to your entity, set payment gateway in
`Type` field and set amount of money to transfer for each click.

## Payment Gateway Plugins
The module allows you to create additional payment gateways by writing custom
plugins. A new plugin must be a PaidGateways plugin and implement
`\Drupal\paid_ads\Plugin\PaidGatewayInterface` interface.
See `\Drupal\paid_ads\Plugin\PaidGateways\PaypalGateway` for example.

## EXAMPLES
`Paid Ads Example` module provides example of a field added to an entity and
view.
