<?php

namespace Drupal\paid_ads\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\paid_ads\Plugin\PaidGatewayInterface;
use Drupal\user\UserInterface;

/**
 * Defines a payment entity.
 *
 * @ContentEntityType(
 *   base_table = "paid_payment",
 *   handlers = {
 *     "list_builder" = "Drupal\paid_ads\Entity\PaidPaymentListBuilder",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   id = "paid_payment",
 *   label = @Translation("Paid Payment"),
 *   links = {
 *     "canonical" = "/paid-payment/{payment}",
 *     "collection" = "/admin/content/paid-payment",
 *   }
 * )
 */
class PaidPayment extends ContentEntityBase implements PaidPaymentInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(array $values = []) {
    if (!isset($values['payment_statuses'])) {
      $values['payment_statuses'] = [PaidPaymentStatuses::CREATED];
    }
    return parent::create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTimeAcrossTranslations() {
    return $this->getChangedTime();
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($id) {
    $this->owner[0]->setValue($id);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $user) {
    $this->owner[0]->setValue($user->id());

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->owner[0]->get('target_id')
      ->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->owner[0]->get('entity')
      ->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['bundle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bundle'))
      ->setReadOnly(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time the payment was last edited.'))
      ->setDisplayOptions(
        'view',
        [
          'type' => 'timestamp',
          'weight' => 0,
        ]
      )
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time the payment was created.'))
      ->setDisplayOptions(
        'view',
        [
          'type' => 'timestamp',
          'weight' => 0,
        ]
      )
      ->setDisplayConfigurable('view', TRUE);

    $fields['currency'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Currency'))
      ->setRequired(TRUE)
      ->setDefaultValue('USD');

    $fields['amount'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Amount'))
      ->setRequired(TRUE);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Payment ID'))
      ->setReadOnly(TRUE);

    $fields['owner'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Payer'))
      ->setDefaultValue(0)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback(static::class . '::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions(
        'view',
        [
          'type' => 'author',
          'weight' => 0,
        ]
      )
      ->setDisplayConfigurable('view', TRUE);

    $fields['payment_method'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Payment method'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['payment_statuses'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Payment statuses'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['options'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Options'));

    $fields['payment_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Payment type'));

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('Universally Unique ID'))
      ->setReadOnly(TRUE);
    $fields['order_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('PayPal order id'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', FALSE);
    return $fields;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Replace with 'approve' or 'confirm'.
   */
  public function execute() {

    $options = Json::decode($this->get('options')->value);
    if (isset($options['onSuccess'])) {
      call_user_func($options['onSuccess'][0], $options['onSuccess'][1] ?? []);
    }
    $this->setPaymentStatus(PaidPaymentStatuses::SUCCESS);
    $this->save();
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentStatuses(array $payment_statuses) {
    $this->get('payment_statuses')
      ->setValue($payment_statuses);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentStatus(string $payment_status) {
    $previous_status = $this->getPaymentStatus();

    if (!$previous_status || $previous_status != $payment_status) {
      $this->get('payment_statuses')
        ->appendItem($payment_status);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentStatuses() {
    $payment_statuses = [];
    foreach ($this->get('payment_statuses') as $field_item) {
      $payment_statuses[] = $field_item->getValue()['value'];
    }

    return array_filter($payment_statuses);
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentStatus() {

    $statuses = $this->getPaymentStatuses();
    if ($statuses) {
      $status = end($statuses);
      return $status;
    }
    else {
      return PaidPaymentStatuses::UNKNOWN;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentMethod() {
    return $this->get('payment_method')
      ->first()->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentMethod(PaidGatewayInterface $payment_gateway) {
    $this->get('payment_method')
      ->setValue($payment_gateway->getPluginId());
    $this->set('bundle', $payment_gateway->getPluginId());
    return $this;
  }

  /**
   * Default value callback for 'owner' base field definition.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity default value for.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field default value for.
   *
   * @return array
   *   An array of default values.
   *
   * @see ::baseFieldDefinitions()
   */
  public static function getCurrentUserId(EntityInterface $entity, FieldDefinitionInterface $field_definition) {
    return [
      \Drupal::currentUser()
        ->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    return $this->get('amount')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAmount(string $amount) {
    $this->set('amount', $amount);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->id();
  }

  /**
   * {@inheritdoc}
   */
  public function setOptions($options) {
    if (is_array($options)) {
      $options = Json::encode($options);
    }

    $this->set('options', $options);
    return $this;
  }

  /**
   * Returns currency.
   *
   * @return string
   *   Currency code.
   */
  public function getCurrency() {
    return $this->currency->value;
  }

  /**
   * Updates PayPal order id for the current entity.
   *
   * @param string $value
   *   New value.
   *
   * @return $this
   *   Self.
   */
  public function setOrderId(string $value) {
    $this->set('order_id', $value);
    return $this;
  }

  /**
   * Returns PayPal order id.
   *
   * @return string
   *   String containing PayPal order id.
   */
  public function getOrderId(): string {
    return (string) $this->order_id->value;
  }

}
