<?php

namespace Drupal\paid_ads\Entity;

/**
 * List of available payment status.
 */
final class PaidPaymentStatuses {

  const CREATED = 'created';

  const PENDING = 'pending';

  const FAILED = 'failed';

  const SUCCESS = 'completed';

  const UNKNOWN = 'unknown';

}
