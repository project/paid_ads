<?php

namespace Drupal\paid_ads\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * List builder for paid payments.
 */
class PaidPaymentListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = [
      'data' => $this->t('ID'),
    ];

    $header['amount'] = [
      'data' => $this->t('Amount'),
    ];

    $header['method'] = [
      'data' => $this->t('Method'),
    ];

    $header['owner'] = [
      'data' => $this->t('Owner'),
    ];

    $header['status'] = [
      'data' => $this->t('Status'),
    ];

    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $payment) {
    /** @var \Drupal\paid_ads\Entity\PaidPaymentInterface $payment */
    $row['data']['id'] = $payment->get('id')->value;
    $row['data']['amount'] = $payment->get('amount')->value ?? 'UNKNOWN';
    $row['data']['method'] = $payment->getPaymentMethod();
    $row['data']['owner']['data'] = [
      '#theme' => 'username',
      '#account' => $payment->getOwner(),
    ];
    $row['data']['status'] = $payment->getPaymentStatus();
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->sort($this->entityType->getKey('id'), 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}
