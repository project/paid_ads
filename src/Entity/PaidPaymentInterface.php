<?php

namespace Drupal\paid_ads\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\paid_ads\Plugin\PaidGatewayInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Interface PaidPaymentInterface.
 *
 * @package Drupal\paid_ads\Entity
 */
interface PaidPaymentInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Executes the payment.
   */
  public function execute();

  /**
   * Returns the timestamp of the entity creation.
   *
   * @return int
   *   Gets created time.
   */
  public function getCreatedTime();

  /**
   * Sets/replaces all statuses without notifications.
   *
   * @param string[] $payment_statuses
   *   List of statuses.
   *
   * @return \Drupal\paid_ads\Entity\PaidPaymentInterface
   *   Return this object.
   */
  public function setPaymentStatuses(array $payment_statuses);

  /**
   * Sets a status.
   *
   * @param string $payment_status
   *   Settable status.
   *
   * @return \Drupal\paid_ads\Entity\PaidPaymentInterface
   *   Return this object.
   */
  public function setPaymentStatus(string $payment_status);

  /**
   * Gets all payment statuses.
   *
   * @return string[]
   *   The statuses are ordered by time with the newest last.
   */
  public function getPaymentStatuses();

  /**
   * Gets the current payment status.
   *
   * @return string
   *   Return status string.
   */
  public function getPaymentStatus();

  /**
   * Gets the payment method plugin.
   *
   * @return string
   *   Return payment method.
   */
  public function getPaymentMethod();

  /**
   * Gets the payment method plugin.
   *
   * @param \Drupal\paid_ads\Plugin\PaidGatewayInterface $payment_gateway
   *   Payment gateway.
   *
   * @return static
   */
  public function setPaymentMethod(PaidGatewayInterface $payment_gateway);

  /**
   * Gets total amount.
   *
   * @return string
   *   Return amount as string.
   */
  public function getAmount();

  /**
   * Sets total amount.
   *
   * @param string $amount
   *   Amount.
   *
   * @return mixed
   *   This object.
   */
  public function setAmount(string $amount);

  /**
   * Gets unique identifier.
   *
   * @return int
   *   ID.
   */
  public function getId();

  /**
   * Returns PayPal order id.
   *
   * @return string
   *   String containing PayPal order id.
   */
  public function getOrderId(): string;

  /**
   * Updates PayPal order id for the current entity.
   *
   * @param string $value
   *   New value.
   *
   * @return $this
   *   Self.
   */
  public function setOrderId(string $value);

  /**
   * Returns currency.
   *
   * @return string
   *   Currency code.
   */
  public function getCurrency();

}
