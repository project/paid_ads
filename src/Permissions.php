<?php

namespace Drupal\paid_ads;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Permissions.
 */
class Permissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * EntityTypeManager injection.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * Gets dynamic permissions for node types.
   *
   * @return array
   *   Array with permissions in as yml format.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPermissions() {
    $permissions = [];
    $node_types = $this->entityTypeManager->getStorage('node_type')
      ->loadMultiple();
    foreach ($node_types as $node_type => $definition) {
      $permissions['paid_ads.field.edit_paid.' . $node_type] = [
        'title' => $this->t('Manage paid field of %node_type.', ['%node_type' => $definition->get('name')]),
        'description' => $this->t('Edit paid field on @node_type node.', ['@node_type' => $node_type]),
      ];
    }
    return $permissions;
  }

  /**
   * Permissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Works with entity types.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   Provides translatable strings. Uses in StringTranslationTrait.
   */
  public function __construct(EntityTypeManager $entityTypeManager, TranslationInterface $string_translation) {
    $this->entityTypeManager = $entityTypeManager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'), $container->get('string_translation'));
  }

}
