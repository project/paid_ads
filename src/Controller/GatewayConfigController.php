<?php

namespace Drupal\paid_ads\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\paid_ads\PaidService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GatewayConfigController.
 *
 * Handle route for list of payments.
 *
 * @package Drupal\paid_ads\Controller
 */
class GatewayConfigController extends ControllerBase {

  /**
   * PaidService injection.
   *
   * @var \Drupal\paid_ads\PaidService
   */
  private $paidService;

  /**
   * PaidController constructor.
   *
   * @param \Drupal\paid_ads\PaidService $paidService
   *   Inject PaidService.
   */
  public function __construct(PaidService $paidService) {
    $this->paidService = $paidService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\paid_ads\PaidService $paidService */
    $paidService = $container->get('paid_ads.service');
    return new static($paidService);
  }

  /**
   * Handle list of payment.
   *
   * @return array
   *   List of payment with operations in table.
   */
  public function gatewayList() {
    $list = $this->paidService->getPluginsList();
    $rows = [];
    foreach ($list as $key => $item) {
      $config['data'] = [
        '#type' => 'operations',
        '#links' => [
          'edit' =>
            [
              'title' => $this->t('Edit'),
              'url' => Url::fromRoute('paid_ads.admin.gateway.gateway',
                ['gateway' => $key]),
            ],
        ],
        '#attached' => [
          'library' => ['core/drupal.ajax'],
        ],
      ];

      $rows[] = [
        'data' => [
          'label' => $item,
          'config' => $config,
        ],
      ];
    }
    return [
      '#type' => 'table',
      '#header' => [
        'label' => $this->t('Label'),
        'config' => $this->t('Configuration'),
      ],
      '#rows' => $rows,
    ];
  }

}
