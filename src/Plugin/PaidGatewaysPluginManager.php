<?php

namespace Drupal\paid_ads\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class PaidGatewaysPluginManager.
 */
class PaidGatewaysPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new PaidGatewaysPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/PaidGateways', $namespaces, $module_handler, 'Drupal\paid_ads\Plugin\PaidGatewayInterface', 'Drupal\paid_ads\Annotation\PaidGateway');
    $this->alterInfo('paid_gateways_plugin_info');
    $this->setCacheBackend($cache_backend, 'paid_gateways_plugins');
  }

}
