<?php

namespace Drupal\paid_ads\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Define an interface for payment gateways.
 */
interface PaidGatewayInterface extends PluginInspectionInterface, PluginFormInterface {

  /**
   * Display form.
   *
   * @param array $option
   *   Options for form.
   *
   * @return array
   *   Form array.
   */
  public function getForm(array $option);

  /**
   * Form for specific configuration.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState instance.
   *
   * @return array
   *   Form array.
   */
  public function getConfigForm(array $form, FormStateInterface $form_state);

  /**
   * Process when payment is created.
   *
   * @param array $context
   *   Context for create payment.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Instance of response.
   */
  public function onCreatePayment(array $context);

  /**
   * Process when payment is confirmed.
   *
   * @param array $context
   *   Context for execute payment.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Instance of response.
   */
  public function onApprovePayment(array $context);

}
