<?php

namespace Drupal\paid_ads\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\paid_ads\PaidService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'paid' formatter.
 *
 * @FieldFormatter(
 *   id = "paid_boolean",
 *   label = @Translation("Paid(boolean)"),
 *   field_types = {
 *     "paid_boolean",
 *   }
 * )
 */
class PaidFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * PaidService injection.
   *
   * @var \Drupal\paid_ads\PaidService
   */
  private $paidService;

  /**
   * {@inheritdoc}
   */
  public function __construct(string $plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, string $label, string $view_mode, array $third_party_settings, PaidService $service) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
    $this->paidService = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['paid_display'] = 'Paid';
    $settings['not_paid_display'] = '';
    $settings['button_settings'] = [];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $btn_settings = $this->getSetting('button_settings');
    $form['not_paid_display'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text for non paid payment'),
      '#default_value' => $this->getSetting('not_paid_display'),
    ];

    $form['paid_display'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text for non paid payment'),
      '#default_value' => $this->getSetting('paid_display'),
    ];
    $form['button_settings'] = [
      '#type' => 'collapsible',
      '#title' => $this->t('Button Settings'),
      '#description' => $this->t(
        'PayPal buttons customization. See @url for examples',
        [
          '@url' => 'https://developer.paypal.com/docs/checkout/integration-features/customize-button/',
        ]
      ),
    ];
    $form['button_settings']['layout'] = [
      '#type' => 'select',
      '#options' => [
        'horizontal' => $this->t('Horizontal'),
        'vertical' => $this->t('Vertical'),
        '' => $this->t('Not Set'),
      ],
      '#default_value' => $btn_settings['layout'] ?? '',
    ];
    $form['button_settings']['color'] = [
      '#type' => 'select',
      '#title' => $this->t('Color'),
      '#options' => [
        'gold' => $this->t('Gold (Recommended)'),
        'blue' => $this->t('Blue (First Alternative)'),
        'silver' => $this->t('Silver'),
        'white' => $this->t('White'),
        'black' => $this->t('Black'),
        '' => $this->t('Not Set'),
      ],
      '#default_value' => $btn_settings['color'] ?? '',
    ];
    $form['button_settings']['shape'] = [
      '#type' => 'select',
      '#title' => $this->t('Shape'),
      '#options' => [
        'rect' => $this->t('Rect'),
        'pill' => $this->t('Pill'),
        '' => $this->t('Not Set'),
      ],
      '#default_value' => $btn_settings['shape'] ?? '',
    ];
    $form['button_settings']['label'] = [
      '#type' => 'select',
      '#title' => $this->t('Label'),
      '#options' => [
        'paypal' => $this->t('Paypal'),
        'checkout' => $this->t('Checkout'),
        'buynow' => $this->t('BuyNow'),
        'pay' => $this->t('Pay'),
        '' => $this->t('Not Set'),
      ],
      '#default_value' => $btn_settings['label'] ?? '',
    ];
    $form['button_settings']['tagline'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show tagline text'),
      '#return_value' => 1,
      '#default_value' => $btn_settings['tagline'] ?? 1,
      '#description' => $this->t(
        'CAUTION! Can fail the script with vertical layout'
      ),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = array_filter($this->getSetting('button_settings'));
    if (isset($settings['tagline'])) {
      $settings['tagline'] = 'Tagline';
    }
    if ($settings['label'] !== 'installment') {
      unset($settings['period']);
    }
    return [implode(', ', $settings)];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    if (!$items->first()->value) {
      $pluginId = $items->getSetting('type');
      $form['label'] = [
        '#markup' => $this->getSetting('not_paid_display'),
      ];
      $form['plugin'] = $this->paidService->buildFormByPluginId(
        $pluginId,
        [
          'amount' => $items->getSetting('amount'),
        ]
      );
      $id = implode(
        '_',
        [
          $items->getFieldDefinition()
            ->getTargetEntityTypeId(),
          $items->getFieldDefinition()
            ->getTargetBundle(),
          $items->getFieldDefinition()
            ->getName(),
          $items->getEntity()
            ->id(),
          1,
        ]
      );
      $form['plugin']['#attributes'] = array_merge(
        $form['plugin']['#attributes'] ?? [],
        [
          'data-entity-type' => $items->getFieldDefinition()
            ->getTargetEntityTypeId(),
          'data-bundle' => $items->getFieldDefinition()
            ->getTargetBundle(),
          'data-field' => $items->getFieldDefinition()
            ->getName(),
          'data-entity-id' => $items->getEntity()
            ->id(),
          'data-field-value' => 1,
          'id' => $id,
        ]
      );
      $form['#attached']['drupalSettings']['paypal_buttons'] = $this->getSetting(
          'button_settings'
        ) ?? [];
      return [$form];
    }
    else {
      return [
        [
          '#theme' => 'paid_boolean_paid',
          '#item' => $items->first()->value,
          '#text' => $this->getSetting('paid_display'),
        ],
      ];
    }
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('paid_ads.service')
    );
  }

}
