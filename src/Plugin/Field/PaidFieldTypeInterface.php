<?php

namespace Drupal\paid_ads\Plugin\Field;

/**
 * Interface PaidFieldTypeInterface.
 *
 * @package Drupal\paid_ads\Plugin\Field
 */
interface PaidFieldTypeInterface {

  /**
   * Handler for success payment execution.
   *
   * @param array $options
   *   Options for executions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function onSuccess(array $options);

  /**
   * Return amount depends on field settings.
   *
   * @param array $options
   *   Options for getting amount.
   *
   * @return |null
   *   Amount or null.
   */
  public static function getAmounts(array $options);

}
