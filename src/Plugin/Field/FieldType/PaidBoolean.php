<?php

namespace Drupal\paid_ads\Plugin\Field\FieldType;

use Drupal;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\paid_ads\Plugin\Field\PaidFieldTypeInterface;

/**
 * Defines the 'paid_boolean' entity field type.
 *
 * @FieldType(
 *   id = "paid_boolean",
 *   label = @Translation("Paid (Boolean)"),
 *   description = @Translation("An entity field containing a paid information in boolean value."),
 *   default_widget = "paid_boolean",
 *   default_formatter = "paid_boolean",
 *   cardinality = 1
 * )
 */
class PaidBoolean extends FieldItemBase implements OptionsProviderInterface, PaidFieldTypeInterface {

  /**
   * PaidService injection.
   *
   * @var \Drupal\paid_ads\PaidService
   */
  private $paidService;

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = [
      'amount' => '0.01',
      'type' => 'none',
    ];
    return $settings + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Boolean value'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['amount'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Amount (USD)'),
      '#default_value' => $this->getSetting('amount'),
      '#required' => TRUE,
      '#element_validate' => [
        [static::class, 'validateFloat'],
      ],
    ];

    $element['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $this->getSetting('type'),
      '#options' => $this->getPaidAdsService()->getPluginsList(),
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * Float validator.
   *
   * @param array $element
   *   Element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   FormState instance.
   * @param array $form
   *   Form array.
   */
  public static function validateFloat(array &$element, FormStateInterface $formState, array &$form) {
    $value = floatval($formState->getValue('settings')['amount']);
    if (!$value) {
      $formState->setError($element);
    }
    $formated_value = number_format(floatval($value), 2, '.', '');
    $formState->setValueForElement($element, $formated_value);
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleValues(AccountInterface $account = NULL) {
    return [0, 1];
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL) {
    return [
      0 => $this->t('Not paid'),
      1 => $this->t('Paid'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableValues(AccountInterface $account = NULL) {
    return [0, 1];
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL) {
    return $this->getPossibleOptions($account);
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = mt_rand(0, 1);
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public static function onSuccess(array $options) {
    $storage = Drupal::entityTypeManager()
      ->getStorage($options['data_entity_type']);
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $storage->load($options['data_entity_id']);
    $entity->set($options['data_field'], $options['data_field_value'])
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function getAmounts(array $options) {
    if ($field_config = FieldConfig::load($options['field_id'])) {
      return $field_config->getSetting('amount');
    }
    return NULL;
  }

  /**
   * Getter for PaidService.
   *
   * @return \Drupal\paid_ads\PaidService
   *   PaidService instance.
   */
  protected function getPaidAdsService() {
    if (!$this->paidService) {
      $this->paidService = Drupal::getContainer()->get('paid_ads.service');
    }
    return $this->paidService;
  }

}
