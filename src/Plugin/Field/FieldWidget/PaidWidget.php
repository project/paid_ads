<?php

namespace Drupal\paid_ads\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'boolean_checkbox' widget.
 *
 * @FieldWidget(
 *   id = "paid_boolean",
 *   label = @Translation("Paid widget"),
 *   field_types = {
 *     "paid_boolean"
 *   },
 *   multiple_values = FALSE
 * )
 */
class PaidWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = array_merge($element, [
      '#type' => 'checkbox',
      '#default_value' => !empty($items[0]->value),
      '#title_display' => 'after',
      '#title' => $this->fieldDefinition->getLabel(),
    ]);
    return $element;
  }


}
