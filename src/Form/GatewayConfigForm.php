<?php

namespace Drupal\paid_ads\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paid_ads\PaidService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a gateway config form.
 */
class GatewayConfigForm extends FormBase {

  /**
   * Injection of PaidService.
   *
   * @var \Drupal\paid_ads\PaidService
   */
  private $paidService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('paid_ads.service'));
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(PaidService $paidService) {
    $this->paidService = $paidService;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paid_gateway_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $gateway = '') {
    $form = $this->paidService->buildConfigFormByPluginId($gateway, $form, $form_state);
    $form_state->setStorage(['gateway' => $gateway]);
    if ($form) {
      return $form;
    }
    throw new NotFoundHttpException();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $gateway = $form_state->get('gateway');
    $this->paidService->getInstance($gateway)
      ->submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $gateway = $form_state->get('gateway');
    $this->paidService->getInstance($gateway)
      ->validateConfigurationForm($form, $form_state);
  }

}
