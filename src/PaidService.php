<?php

namespace Drupal\paid_ads;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\paid_ads\Plugin\PaidGatewaysPluginManager;

/**
 * Class PaidService.
 */
class PaidService {

  use StringTranslationTrait;

  /**
   * PaidGatewaysPluginManager injection.
   *
   * @var \Drupal\paid_ads\Plugin\PaidGatewaysPluginManager
   */
  private $pluginManager;

  /**
   * Drupal logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * PaidService constructor.
   *
   * @param \Drupal\paid_ads\Plugin\PaidGatewaysPluginManager $pluginManager
   *   PluginManager instance.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Injected Drupal logger channel factory.
   */
  public function __construct(PaidGatewaysPluginManager $pluginManager, LoggerChannelInterface $logger) {
    $this->pluginManager = $pluginManager;
    $this->logger = $logger;
  }

  /**
   * Gets list of PaidGateways plugins.
   *
   * @return array
   *   Return list of plugins.
   */
  public function getPluginsList(): iterable {
    $plugins = [];
    foreach ($this->pluginManager->getDefinitions() as $plugin) {
      $plugins[$plugin['id']] = $plugin['label']->render();
    }
    return $plugins;
  }

  /**
   * Request display form from plugin.
   *
   * @param string $pluginId
   *   Id of plugin.
   * @param array $options
   *   Options for form.
   *
   * @return array
   *   Render form array.
   */
  public function buildFormByPluginId($pluginId, array $options) {
    $plugin = $this->getInstance($pluginId);
    if ($plugin) {
      return $plugin->getForm($options);
    }
    return [];
  }

  /**
   * Request configuration form from plugin.
   *
   * @param string $pluginId
   *   Id of plugin.
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   FormState instance.
   *
   * @return array
   *   Render form array.
   */
  public function buildConfigFormByPluginId($pluginId, array $form, FormStateInterface $formState) {
    $plugin = $this->getInstance($pluginId);
    if ($plugin) {
      return $plugin->getConfigForm($form, $formState);
    }
    return [];
  }

  /**
   * Gets PaidGatewaysPlugin instance by plugin id.
   *
   * @param string $pluginId
   *   Id of plugin.
   *
   * @return \Drupal\paid_ads\Plugin\PaidGatewaysPluginBase|null
   *   PaidGatewaysPlugin instance.
   */
  public function getInstance($pluginId) {
    try {
      if ($this->pluginManager->hasDefinition($pluginId)) {
        /* @var $plugin \Drupal\paid_ads\Plugin\PaidGatewaysPluginBase */
        $plugin = $this->pluginManager->createInstance($pluginId);
        return $plugin;
      }
      else {
        $this->logger->warning($this->t('Could not find a plugin with id: @id', ['@id' => $pluginId]));
        return NULL;
      }
    }
    catch (PluginException $e) {
      $this->logger->error($this->t('Plugin exception raised with message: @msg on retrieving plugin @name', [
        '@msg' => $e->getMessage(),
        '@name' => $pluginId,
      ]));
      return NULL;
    }
  }

  /**
   * Get amount by field type class.
   *
   * @param string $class
   *   Class namespace.
   * @param array $options
   *   Options for callback.
   *
   * @return string
   *   Amount.
   */
  public function getAmountByFieldClass($class, array $options) {
    return call_user_func([$class, 'getAmounts'], $options);
  }

}
