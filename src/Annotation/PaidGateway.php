<?php

namespace Drupal\paid_ads\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define a Paid gateways item annotation object.
 *
 * @Annotation
 */
class PaidGateway extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Getter for label value.
   *
   * @return string
   *   Return label value.
   */
  public function getLabel() {
    return $this->definition['label'] ?? '';
  }

}
