<?php

namespace Drupal\Tests\paid_ads\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class SettingsFormsTest.
 *
 * @group paid_ads
 */
class SettingsFormsTest extends BrowserTestBase {

  protected static $modules = ['paid_ads', 'system'];

  /**
   * Tests if config form is accessible and works.
   */
  public function testPayPalGatewayConfigForm() {
    $modes = [
      'https://api.sandbox.paypal.com',
      'https://api.paypal.com',
    ];
    $options = [
      'mode' => $modes[array_rand($modes, 1)],
      'client_id' => $this->randomString(32),
      'secret_id' => $this->randomString(32),
    ];
    $this->config('paid_ads.settings')
      ->set('paypal_gateway', $options)
      ->save();

    $admin = $this->drupalCreateUser(['paid_ads.payment.administer']);
    $this->drupalLogin($admin);
    $this->drupalGet('/admin/config/paid-gateways/paypal_gateway');
    $this->assertSession()
      ->fieldValueEquals('form[mode]', $options['mode']);
    $this->assertSession()
      ->fieldValueEquals('form[client_id]', $options['client_id']);
    $this->assertSession()
      ->fieldValueEquals('form[secret_id]', $options['secret_id']);
  }

}
