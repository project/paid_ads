<?php

namespace Drupal\Tests\paid_ads\Functional;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for PaidBoolean field.
 *
 * @group in_progress
 */
class PaidBooleanFieldTest extends BrowserTestBase {

  static protected $modules = ['entity_test', 'paid_ads'];

  protected $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->webUser = $this->drupalCreateUser(
      ['view test entity', 'administer entity_test content', 'access content', 'paid_ads.payment.administer']
    );
    $this->drupalLogin($this->webUser);
  }

  public function testFieldWidget() {
    //    $this->drupalCreateContentType(['type' => 'article']);

    $field_type = 'paid_boolean';
    $widget_type = 'paid_boolean';
    // Create a field.
    $field_name = mb_strtolower($this->randomMachineName());
    $field_storage = FieldStorageConfig::create(
      [
        'field_name' => $field_name,
        'type' => $field_type,
        'entity_type' => 'entity_test',
      ]
    );
    $field_storage->save();

    $result = FieldConfig::create(
      [
        'entity_type' => 'entity_test',
        'field_storage' => $field_storage,
        'bundle' => 'entity_test',
        'label' => $this->randomMachineName() . '_label',
        'settings' => [
          'type' => 'paypal_gateway',
          'amount' => (string) random_int(10, 100),
        ],
      ]
    )
      ->save();
    self::assertTrue(in_array($result, [SAVED_NEW, SAVED_UPDATED]));
    // Widgets.
    $result = entity_get_form_display('entity_test', 'entity_test', 'default')
      ->setComponent($field_name, ['type' => $widget_type])
      ->save();
    self::assertTrue(in_array($result, [SAVED_NEW, SAVED_UPDATED]));

    $result = entity_get_display('entity_test', 'entity_test', 'full')
      ->setComponent($field_name)
      ->save();
    self::assertTrue(in_array($result, [SAVED_NEW, SAVED_UPDATED]));

    // Display creation form.
    $this->drupalGet('entity_test/add');
    $this->assertSession()
      ->checkboxNotChecked("{$field_name}[0][value]");

//     Submit with some value.
        $value = rand(0, 1) ? 1 : '';
        $edit = [
          "{$field_name}[0][value]" => $value,
        ];
        $this->drupalPostForm(NULL, $edit, t('Save'));
        preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
        $id = $match[1];
        $this->assertSession()->pageTextContains(
          t('entity_test @id has been created.', ['@id' => $id])
        );

        // Display the entity.
        $entity = EntityTest::load($id);
        $display = entity_get_display(
          $entity->getEntityTypeId(),
          $entity->bundle(),
          'full'
        );
        $content = $display->build($entity);
        $rendered_entity = \Drupal::service('renderer')->renderRoot($content);
        if ($value) {
          $this->assertContains('paid', (string) $rendered_entity);
        }
        else {
          $this->assertContains('paypal-button', (string) $rendered_entity);
        }
        $this->assertContains($value, (string) $rendered_entity);
  }

}
