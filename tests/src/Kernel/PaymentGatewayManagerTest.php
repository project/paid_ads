<?php

namespace Drupal\Tests\paid_ads\Kernel;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\paid_ads\PaidService;
use Drupal\paid_ads\Plugin\PaidGatewayInterface;

/**
 * Class PaymentGatewayManagerTest.
 *
 * @group paid_ads
 */
class PaymentGatewayManagerTest extends KernelTestBase {

  protected static $modules = ['paid_ads'];

  /**
   * Tests the service exists.
   */
  public function testServiceExists() {
    self::assertInstanceOf(PaidService::class, $this->container->get('paid_ads.service'));
    self::assertInstanceOf(PluginManagerInterface::class, $this->container->get('plugin.manager.paid_gateways'));
  }

  /**
   * Tests that plugins really exist and have correct names.
   */
  public function testPaypalPluginExists() {
    /** @var \Drupal\paid_ads\Plugin\PaidGatewaysPluginManager $manager */
    $manager = $this->container->get('plugin.manager.paid_gateways');
    $instance = $manager->createInstance('paypal_gateway');

    self::assertInstanceOf(PaidGatewayInterface::class, $instance);
  }

}
